## [赛泰先生](https://space.bilibili.com/1936685014) B站实战教学视频配套源代码合集

### 源代码说明
本合集中大部分源代码在UP主实战教学视频中均有涉及，是视频中重要的素材，非常值得广大学习Java的技术朋友系统学习。

正如视频中演示，所见即所得，每份代码均能够正常运行。UP所使用的系统是Mac、运行环境是Java8。当然由于系统差异、环境差异，偶尔会有部分代码运行异常，但是作为范例代码参考也是非常值得推荐的。

每份源代码包含`库表结构`、`内置演示数据`、`使用说明文档`等。

> 每份源代码仅需要`¥1元钱`，快添加UP微信`dream4s`（注明来意），打赏红包，领取源代码吧

### 实战Java技术分享的特点
#### 视频时长适宜
每个视频时长在10～20分钟左右，时长非常紧凑，开门见山，不啰嗦直接上菜，节约广大技术朋友的宝贵时间。

#### 一个萝卜一个坑
一般来说，一个视频会讲解一个主题内容，比较复杂的主题会以`合集`的形式呈现，从简单到复杂，从具体到抽象，循序渐进挖掘技术点。学到便是赚到。

#### 代码优化
UP是实战型选手，不是PPT型选手，因此除了进行简单的代码教学之外，更多的时间是帮助学习者如何进行代码优化。

PPT选手只会纸上谈兵，PPT上口若悬河，实际动手，困难重重。说一句现实的话，老板让你来是听你讲PPT呢，还是让你写代码呢。为了就业，为了加薪，你也要成为一名实战性选手。

`代码优化`不是必须环节，但是会让你觉得自己的工作有意义，这点非常重要！如果你是刚刚接触Java开发或者是应届生，可能体会不到，3年工作经验或者5年工作经验的开发，如果你每天的工作任务是生产`屎山`，那么会感觉工作除了为了挣钱外毫无意义、毫无乐趣。

> 每个人都不喜欢屎山，可是身体很诚实，眼睛一睁开，机械的制造新屎山。

### 后期内容安排
#### 基础内容讲解
后续视频分享，会更多的投入精力照顾刚入行的技术朋友，帮助这部分群体顺利入行。

内容选择的话，会选择比较大众化的题材，必须`HuTool工具包`的使用，`Java Guide`等讲解，或者其它大众化需求的内容。这部分内容上手难度较低，我们会从实战型视角，带给你不一样的学习体验。

哪些内容重点学，哪些内容了解一下即可；哪些内容经常使用，哪些内容几乎用户到，帮助你把书读薄。举例说明在Java语言中，很多资料还在讲`HashMap`与`HashTable`的区别是什么，事实上，也许你工作三五年以上，都不会在项目中见到`HashTable`集合，那你还学它干嘛，留点精力看看别的内容不香吗。

#### 持续代码优化
代码优化会持续进行，这也是`UCode CMS`项目的价值所在。

### 源代码增值服务
对于视频中分享的源代码，有需要的话可提供增值服务。

##### 技术答疑
针对具体实际学习过程中遇到的问题进行`技术答疑`，帮助你更加顺利的学习消化技术。

##### 远程Debug
除了普通的技术答疑之外，也提供远程控屏，实操解决环境问题，帮助你顺利复现视频中演示内容

##### 功能定制
如果你收到UP主视频分享的启发，结合自己具体场景，有功能定制的需求，也可咨询。免费提供方案可行性技术分析。

### 附录
更多内容，请通过Java技术博客、开源项目、B站视频学习。
![赛泰先生教学福利包](赛先生和泰先生实战教学福利包.jpeg)

