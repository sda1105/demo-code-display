### [视频讲解](https://www.bilibili.com/video/BV1Gt4y1K7x1)

### 内容提示
本示例完成了MybatisPlus Lambda风格的分组聚合函数查询，以`count`函数为范例，讲述分组函数的查询实现过程，后续视频会拓展到其它函数。